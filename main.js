const { app, BrowserWindow } = require('electron');

let mainWindow

function beginStartup() {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        autoHideMenuBar: true,
    });

    mainWindow.on('closed', () => {
        mainWindow = null
        app.quit();
    });

    mainWindow.loadFile('index.html');
}

app.on('ready', beginStartup);